/*
 * Biblioteca com todas operacoes com lista encadeada do sistema
 *
 */

#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include <vector>
#include <Persistencia/controle.h>

using namespace std;

template <class T>
class Lista{
private:

    typedef struct celula{
        T item;
        celula* next;
    }* tipoCelula;

    tipoCelula head; //Cabeça da lista
    tipoCelula aux;  //Auxiliar

public:
    Lista(){
        head = NULL;
        aux = NULL;
    }

    void Insere(T novo, Controle &controle){
        int atr = 0;
        int cmp = 0;

        //Cria o objeto de nó
        tipoCelula n = new celula; atr++;
        n->next = NULL; atr++;
        n->item = novo; atr++;

        //Roda a lista para adiconar no ultimo
        cmp++;
        if(head != NULL){
            aux = head; atr++;
            while(aux->next != NULL){ cmp++;
                aux = aux->next; atr++;
            }

            aux->next = n; atr++;
        }
        //Se não, adiciona na cabeça
        else{
            head = n; atr++;
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);
    }

    bool Deleta(int codigo, Controle &controle){
        int atr = 0;
        int cmp = 0;

        tipoCelula delItem = NULL; atr++;
        tipoCelula delAux = head; atr++;
        aux = head; atr++;

        //Se está na cabeça
        cmp++;
        if(head->item.getCodigo() == codigo){
            delItem = head; atr++;
            head = head->next; atr++;
            delete delItem;

            controle.setAtribuicoes(atr);
            controle.setComparacoes(cmp);
            controle.setOperacoes(1);

            return true;

        }else{
            //Roda a lista até encontrar ou até apontar pra null
            while(aux != NULL && aux->item.getCodigo() != codigo){
                cmp = cmp+2;
                delAux = aux; atr++;
                aux = aux->next; atr++;
            }
            cmp++;
            if(aux == NULL){
                delete delItem; atr++;

                controle.setAtribuicoes(atr);
                controle.setComparacoes(cmp);
                controle.setOperacoes(1);

                return false; //Não está na lista
            }
            else{
                delItem = aux; atr++;
                aux = aux->next; atr++;
                delAux->next = aux; atr++;
                delete delItem; atr++;

                controle.setAtribuicoes(atr);
                controle.setComparacoes(cmp);
                controle.setOperacoes(1);

                return true;
            }
        }
    }

    vector<T> ListaTodos(){
        aux = head;

        vector<T> lista;

        while(aux != NULL){
            lista.push_back(aux->item);
            aux = aux->next;
        }

        return lista;
    }

    T Consulta(int codigo){
        aux = head;

        //Roda a lista até encontrar ou até apontar pra null
        while(aux != NULL && aux->item.getCodigo() != codigo){
            aux = aux->next;
        }

        if(aux == NULL){
            cout << codigo << " não está na lista\n";
            return T();
        }
        else{
            return aux->item;
        }
    }

    T ConsultaFuncionario(string chave){
        aux = head;

        //Roda a lista até encontrar ou até apontar pra null
        while(aux != NULL && aux->item.getLogin() != chave){
            aux = aux->next;
        }

        if(aux == NULL){
            return T();
        }
        else{
            return aux->item;
        }
    }

    void Atualiza(T item){
        aux = head;

        //Roda a lista até encontrar ou até apontar pra null
        while(aux != NULL && aux->item.getCodigo() != item.getCodigo()){
            aux = aux->next;
        }

        aux->item = item;

    }

    vector<T> Busca(string query, int identificador){
        aux = head;

        vector<T> retornoBusca;

        switch(identificador){

        //Busca livro por titulo
        case 1:
            while(aux != NULL){


                //Esse string.compare deveria funcionar, precisamos achar
                //alguma forma de comparar 2 strings tipo do SQL, pois
                //com o == " ab" é diferente de "ab"

                //if(aux->livro.getEditora().compare("query") != 0){
                //    retornoBusca.push_back(aux->livro);
                //}

                if(aux->livro.getTitulo() == query){
                    retornoBusca.push_back(aux->livro);
                }

                aux = aux->next;
            }
            break;
        //Autor
        case 2:
            while(aux != NULL){

                if(aux->livro.getAutor() == query){
                    retornoBusca.push_back(aux->livro);
                }

                aux = aux->next;
            }
            break;
        //Editora
        case 3:
            while(aux != NULL){

                if(aux->livro.getEditora() == query){
                    retornoBusca.push_back(aux->livro);
                }

                aux = aux->next;
            }
            break;
        default:
            break;
        }

        return retornoBusca;

    }

};

#endif // LISTA_H
