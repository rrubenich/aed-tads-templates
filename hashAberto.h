/*
 * Hashing Aberto utilizando uma string do item na função hash
 * (Usado para Usuario e Livro e Emprestimos)
 *
 * L = Livre
 * O = Ocupado
 */

#ifndef HASHABERTO_H
#define HASHABERTO_H

#include <string>
#include <iostream>
#include <vector>
#include "controle.h"
#include "data.h"

using namespace std;

template <class T>
class HashAberto{

private:

    static const int tamanhoTabela = 59;

    typedef struct celula{
        T item;
        celula* next;
        string marcador;
    }* tipoCelula;

    tipoCelula HashTable[tamanhoTabela];
    tipoCelula aux;


public:
    HashAberto(){

        //Número primo mais próximo de 55(teto) (Intervalo entre z-A)
        //tamanhoTabela = 59;
        for(int i = 0; i < tamanhoTabela; i++){
            HashTable[i] = new celula;
            HashTable[i]->marcador = "L";
            HashTable[i]->next = NULL;
        }
    }

    int funcaoHash(string chave){
        int hash, posicao;

        hash = (int)chave[0];
        hash = hash + (int)chave[1];

        posicao = hash % tamanhoTabela;

        return posicao;
    }

    void Insere(T item, string chave, Controle &controle){
        int atr = 0;
        int cmp = 0;
        int posicao = funcaoHash(chave); atr++;

        //Se a cabeça estiver vazia
        cmp++;
        if(HashTable[posicao]->marcador == "L"){
            HashTable[posicao]->item = item; atr++;
            HashTable[posicao]->marcador = ""; atr++;
        }
        else{
            aux = HashTable[posicao]; atr++;
            tipoCelula novo = new celula; atr++;
            novo->item = item; atr++;
            novo->next = NULL; atr++;

            while(aux->next != NULL){ cmp++;
                aux = aux->next; atr++;
            }

            aux->next = novo; atr++;
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);
    }


    //Consulta qualquer item pelo seu código e chave (titulo)
    T Consulta(string chave, int codigo){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(aux->item.getCodigo() == codigo || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta pelo titulo do livro
    T ConsultaLivro(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getTitulo()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta pelo nome do usurio
    T ConsultaUsuario(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getNome()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    //Consulta pelo login do funcionario
    T ConsultaFuncionario(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getLogin()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }
    //Consulta pelo nome do Usuario
    T ConsultaEmprestimo(string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){
            while(chave.compare(aux->item.getLivro().getTitulo()) != 0 || aux->next != NULL){
                aux = aux->next;
            }
            return aux->item;
        }
        else{
            return T();
        }
    }

    vector<T> ListaTodos(){
        vector<T> lista;

        for(int i = 0; i < tamanhoTabela;i++){
            aux = HashTable[i];

            if(!(HashTable[i]->marcador == "L")){
                lista.push_back(aux->item);
                while(aux->next != NULL){
                    aux = aux->next;
                    lista.push_back(aux->item);
                }
            }
        }

        return lista;
    }

    vector<T> ListaTodos(Controle controle){
        int atr = 0;
        int cmp = 0;
        int i;

        vector<T> lista;

        for(i = 0, atr++; (i < tamanhoTabela) && cmp++;i++, atr++){

            aux = HashTable[i]; atr++;

            cmp++;
            if(!(HashTable[i]->marcador == "L")){

                lista.push_back(aux->item);
                cmp++;
                atr++;
                while(aux->next != NULL){
                    cmp++;
                    aux = aux->next;
                    lista.push_back(aux->item);
                    atr+2;
                }
            }
        }

        controle.setAtribuicoes(atr);
        controle.setComparacoes(cmp);
        controle.setOperacoes(1);

        return lista;
    }


    bool Edita(T item, string chave){
        int posicao = funcaoHash(chave);
        aux = HashTable[posicao];

        if(!(HashTable[posicao]->marcador == "L")){

            while(aux->item.getCodigo() == item.getCodigo()){
                HashTable[posicao]->item = item;
                return true;
            }
        }
        else{
            return false;
        }
    }


    //Deleta usa a ID e a chave do objeto!
    bool Deleta(int codigo, string chave){

        int posicao = funcaoHash(chave);

        tipoCelula auxDel1;
        tipoCelula auxDel2;

        if(HashTable[posicao]->marcador == "L"){
            return false;
        }

        else if(HashTable[posicao]->item.getCodigo() == codigo && HashTable[posicao]->next == NULL){
            HashTable[posicao]->marcador = "L";
        }
        else if(HashTable[posicao]->item.getCodigo() == codigo){

            aux = HashTable[posicao];
            HashTable[posicao] = HashTable[posicao]->next;
            delete aux;
            return false;
        }


        else{
            auxDel1 = HashTable[posicao]->next;
            auxDel2 = HashTable[posicao];

            while(auxDel1 != NULL && auxDel1->item.getCodigo() != codigo){
                auxDel2 = auxDel1;
                auxDel1 = auxDel1->next;
            }

            if(auxDel1 == NULL){
                return false;
            }
            else{
                aux = auxDel1;
                auxDel1 = auxDel1->next;
                auxDel2->next = auxDel1;

                delete aux;
                return true;
            }
        }
    }
};

#endif // HASHABERTO_H
